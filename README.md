BDSup2Sub++
===========

BDSup2Sub++ (c) is a port of the original BDSup2Sub (c) 2009-2013 by
0xdeadbeef with incorporating enhancements from  Miklos Juhasz.

It is a subtitle conversion tool for image based stream formats with
scaling capabilities and some other nice features.
