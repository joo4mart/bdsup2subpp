# qmake configuration for package bdsup2subpp

#check Qt version
QT_VERSION = $$[QT_VERSION]
QT_VERSION = $$split(QT_VERSION, ".")
QT_VMAJOR  = $$member(QT_VERSION, 0)

QT        += core xml gui
greaterThan(QT_VMAJOR, 4) {
QT        -= gui
QT        += widgets
}
lessThan(QT_VMAJOR, 4) {
error(Found Qt $$QT_VMAJOR - please install at least Qt 4 - Abort!)
}

CONFIG    += qt console
DEFINES   += BUILD_QXT_CORE
QMAKE_CXXFLAGS += -std=c++14
# final binary filename
TARGET     = bdsup2sub++
# build an application
TEMPLATE   = app

DESTDIR    = bin
OBJECTS_DIR = build
MOC_DIR    = build
RCC_DIR    = build
UI_DIR     = build

# path used by #include directives
INCLUDEPATH += src

SOURCES += \
    src/main.cpp \
    src/bdsup2sub.cpp \
    src/zoomableimagearea.cpp \
    src/editpane.cpp \
    src/Filters/trianglefilter.cpp \
    src/Filters/mitchellfilter.cpp \
    src/Filters/lanczos3filter.cpp \
    src/Filters/hermitefilter.cpp \
    src/Filters/bsplinefilter.cpp \
    src/Filters/bicubicfilter.cpp \
    src/Filters/bellfilter.cpp \
    src/Filters/filterop.cpp \
    src/Subtitles/subdvd.cpp \
    src/Subtitles/palette.cpp \
    src/Subtitles/bitmap.cpp \
    src/Subtitles/subtitleprocessor.cpp \
    src/Subtitles/subpicture.cpp \
    src/Subtitles/erasepatch.cpp \
    src/Subtitles/subpicturedvd.cpp \
    src/Subtitles/imageobjectfragment.cpp \
    src/progressdialog.cpp \
    src/Subtitles/supdvd.cpp \
    src/Tools/filebuffer.cpp \
    src/conversiondialog.cpp \
    src/Filters/filters.cpp \
    src/Subtitles/palettebitmap.cpp \
    src/Tools/timeutil.cpp \
    src/Subtitles/suphd.cpp \
    src/Subtitles/supbd.cpp \
    src/Subtitles/subpicturebd.cpp \
    src/Subtitles/paletteinfo.cpp \
    src/Subtitles/substreamdvd.cpp \
    src/Subtitles/subpicturehd.cpp \
    src/Tools/bitstream.cpp \
    src/Subtitles/supxml.cpp \
    src/Subtitles/subpicturexml.cpp \
    src/Tools/quantizefilter.cpp \
    src/exportdialog.cpp \
    src/Tools/numberutil.cpp \
    src/editdialog.cpp \
    src/helpdialog.cpp \
    src/colordialog.cpp \
    src/framepalettedialog.cpp \
    src/movedialog.cpp \
    src/Subtitles/imageobject.cpp \
    src/qxtglobal.cpp \
    src/qxtcommandoptions.cpp

HEADERS  += \
    src/bdsup2sub.h \
    src/zoomableimagearea.h \
    src/editpane.h \
    src/types.h \
    src/Filters/trianglefilter.h \
    src/Filters/mitchellfilter.h \
    src/Filters/lanczos3filter.h \
    src/Filters/hermitefilter.h \
    src/Filters/filter.h \
    src/Filters/bsplinefilter.h \
    src/Filters/bicubicfilter.h \
    src/Filters/bellfilter.h \
    src/Filters/filterop.h \
    src/Subtitles/substream.h \
    src/Subtitles/substreamdvd.h \
    src/Subtitles/subdvd.h \
    src/Subtitles/palette.h \
    src/Subtitles/bitmap.h \
    src/Subtitles/subtitleprocessor.h \
    src/Subtitles/subpicture.h \
    src/Subtitles/erasepatch.h \
    src/Subtitles/subpicturedvd.h \
    src/Subtitles/imageobjectfragment.h \
    src/progressdialog.h \
    src/Subtitles/supdvd.h \
    src/Tools/filebuffer.h \
    src/conversiondialog.h \
    src/Filters/filters.h \
    src/Subtitles/palettebitmap.h \
    src/Tools/timeutil.h \
    src/Subtitles/suphd.h \
    src/Subtitles/supbd.h \
    src/Subtitles/subpicturebd.h \
    src/Subtitles/imageobject.h \
    src/Subtitles/paletteinfo.h \
    src/Subtitles/subpicturehd.h \
    src/Tools/bitstream.h \
    src/Subtitles/supxml.h \
    src/Subtitles/subpicturexml.h \
    src/Tools/quantizefilter.h \
    src/exportdialog.h \
    src/Tools/numberutil.h \
    src/editdialog.h \
    src/helpdialog.h \
    src/colordialog.h \
    src/framepalettedialog.h \
    src/movedialog.h \
    src/qxtglobal.h \
    src/qxtcommandoptions.h

FORMS += \
    src/bdsup2sub.ui \
    src/progressdialog.ui \
    src/conversiondialog.ui \
    src/exportdialog.ui \
    src/editdialog.ui \
    src/helpdialog.ui \
    src/colordialog.ui \
    src/framepalettedialog.ui \
    src/movedialog.ui

RESOURCES += \
    src/bdsup2sub.qrc

RC_FILE = \
    src/bdsup2sub.rc
